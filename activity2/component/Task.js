import React from "react";
import { View, Text, StyleSheet } from "react-native";

const Task = (props) => {
    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <Text style={styles.circle}>
                </Text>
                <Text style={styles.itemText}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#FFFFFF',
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingVertical: 50,
        borderWidth: 1,
        borderColor: '#000000',
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    circle: {
        width: 27,
        height: 27,
        backgroundColor: '#FF0000',
        borderWidth: 3,
        borderColor: '#000000',
        borderRadius: 60,
        marginRight: 15,
        textAlign: 'center',
        color: "#ffffff"
    },
    itemText: {
        maxWidth: '85%',
        color: '#000000',
        fontWeight: 'bold',
        fontSize: 20,
    },
});

export default Task;