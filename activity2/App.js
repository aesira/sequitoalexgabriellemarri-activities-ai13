import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Keyboard, ScrollView } from 'react-native';
import Task from './component/Task';

export default function App() {
    const [task, setTask] = useState();
    const [taskItems, setTaskItems] = useState([]);
    const handleAddTask = () => {
        Keyboard.dismiss();
        setTaskItems([...taskItems, task])
        setTask("");
    };
    const completeTask = (index) => {
        let itemsCopy = [...taskItems];
        itemsCopy.splice(index, 1);
        setTaskItems(itemsCopy)
    };

    return (
        <View style={styles.container}>
            <TextInput style={styles.txtField} placeholder={'Task'} value={task} onChangeText={text => setTask(text)} />
            <View style={styles.btnComponent}>
                <TouchableOpacity onPress={() => handleAddTask()}>
                    <Text style={styles.btnAdd}>Add Task</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.scrollComponent} contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps='handled'>
                <View style={styles.items}>
                    {
                        taskItems.map((item, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => completeTask(index)}>
                                    <Task text={item} />
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 60,
        flex: 1,
        position: 'relative',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    scrollComponent: {
        marginTop: 20,
        width: '90%',
    },
    txtField: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#000000',
        borderWidth: 1,
        width: '90%',
    },
    btnComponent: {
        marginTop: 20,
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderColor: '#000000',
        borderWidth: 1,
        borderRadius: 6,
        width: '90%',
    },
    btnAdd: {
        paddingHorizontal: 15,
        textAlign: 'center',
        color: '#000000',
    },
});
