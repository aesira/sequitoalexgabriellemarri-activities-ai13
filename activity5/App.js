import { StatusBar } from 'expo-status-bar';
 import { useState } from "react";
 import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default function App() {
 const [keys, setKeys] = useState([]);
 const [result, setResult] = useState("");
 let combination = "";
 function getResult() {
   for (let i=0; i < keys.length; i++) {
     combination = combination + keys[i];
   }
   const executeStringNumber = eval(combination);
   setResult("=" + executeStringNumber);
 } 
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.container}>
      <Text style={styles.output}>
         
        {keys}
        {result}
      </Text>
      </View>
      <View style={styles.container_keys}>
      <TouchableOpacity onPress={() => setKeys(...keys,([ ]))}>
          <Text style={styles.keys}>   C</Text>
         </TouchableOpacity>
          <TouchableOpacity onPress={() => setKeys([...keys, "%"])}>
        <Text style={styles.keys}>   %</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=> setKeys(keys.slice(1, -1))}>
        <Text style={styles.keys}> DEL</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setKeys([...keys, "/"])}>
        <Text style={styles.keys}>    /</Text>
       </TouchableOpacity>
         </View>
         <View style={styles.container_keys}>          
        <TouchableOpacity onPress={() => setKeys([...keys, "7"])}>
          <Text style={styles.keys}>   7</Text>
         </TouchableOpacity>
        <TouchableOpacity onPress={() => setKeys([...keys, "8"])}>
          <Text style={styles.keys}>   8</Text>
       </TouchableOpacity>
       <TouchableOpacity onPress={() => setKeys([...keys, "9"])}>
        <Text style={styles.keys}>   9</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setKeys([...keys, "+"])}>
        <Text style={styles.keys}>    +</Text>
       </TouchableOpacity>
     </View>
     <View style={styles.container_keys}>
        <TouchableOpacity onPress={() => setKeys([...keys, "4"])}>
          <Text style={styles.keys}>   4</Text>
         </TouchableOpacity>
        <TouchableOpacity onPress={() => setKeys([...keys, "5"])}>
          <Text style={styles.keys}>   5</Text>
       </TouchableOpacity>
       <TouchableOpacity onPress={() => setKeys([...keys, "6"])}>
        <Text style={styles.keys}>   6</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setKeys([...keys, "-"])}>
        <Text style={styles.keys}>    -</Text>
       </TouchableOpacity>
    
   </View>
   <View style={styles.container_keys}>
        <TouchableOpacity onPress={() => setKeys([...keys, "1"])}>
          <Text style={styles.keys}>   1</Text>
         </TouchableOpacity>
        <TouchableOpacity onPress={() => setKeys([...keys, "2"])}>
          <Text style={styles.keys}>   2</Text>
       </TouchableOpacity>
       <TouchableOpacity onPress={() => setKeys([...keys, "3"])}>
        <Text style={styles.keys}>   3</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setKeys([...keys, "*"])}>
        <Text style={styles.keys}>    x</Text>
       </TouchableOpacity>
     
   </View>
   <View style={styles.container_keys}>
        <TouchableOpacity onPress={() => setKeys([...keys, "("])}>
          <Text style={styles.keys}>    (</Text>
         </TouchableOpacity>
        <TouchableOpacity onPress={() => setKeys([...keys, "0"])}>
          <Text style={styles.keys}>    0</Text>
       </TouchableOpacity>
       <TouchableOpacity onPress={() => setKeys([...keys, ")"])}>
        <Text style={styles.keys}>    )</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => getResult()}>
          <Text style={styles.keys}>    =</Text>
       </TouchableOpacity> 

     
       </View>
       <View style={styles.container_keys}>
      <TouchableOpacity onPress={() => setKeys([...keys, "00"])}>
        <Text style={styles.keys}>   00</Text>
       </TouchableOpacity>
       <TouchableOpacity onPress={() => setKeys([...keys, "."])}>
        <Text style={styles.keys}>   .</Text>
       </TouchableOpacity>
       </View>
       </View>
       
  );
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#dcbee8",
    alignItems: "center",
    justifyContent: "center",
    margin: 30,
    padding: 25,
  },
  keys:{
    display: "flex",
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "center", 
    borderWidth: 1,
    borderColor: '#aaa',
    fontSize: 30,
    color: '#060108',
    width: 70,
    height: 60,
  },
  container_keys: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    padding: 3,  
  },
  output: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 35,
    color: "#060108",
    backgroundColor: "#dcbee8",
    width: "100%",
  }
});